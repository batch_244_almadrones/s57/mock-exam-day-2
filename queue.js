let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}

function enqueue(text) {
  collection[size()] = text;
  return collection;
}

function dequeue() {
  let removedFirstInCollection = [];
  if (size() > 0) {
    for (let i = 1; i < size(); i++) {
      removedFirstInCollection[i - 1] = collection[i];
    }
  }
  collection = removedFirstInCollection;
  return collection;
}

function front() {
  return collection[0];
}

function size() {
  let count = 0;
  while (collection[count] != undefined) {
    count++;
  }
  return count;
}

function isEmpty() {
  if (collection[0] == undefined) {
    return true;
  } else {
    return false;
  }
}

module.exports = { print, enqueue, dequeue, front, size, isEmpty };
